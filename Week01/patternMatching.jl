function patternMatching(dan::String, k::Integer)
    dic = Dict{String, Array{Integer, 1}}()
    for i = 1:(length(dna)-k+1)
        kmer = dna[i:i+k-1]
        if haskey(dic, kmer)
            push!(dic[kmer], i)
        else
            dic[kmer] = [i]
        end
    end
    return dic        
end

file = open("/home/chang/Documents/Bioinformatics/Week01/testSeq.txt", "r")
pattern = readline(file)
dna     = readline(file)
close(file)

kmersDic = patternMatching(dna, length(pattern))

open("/home/chang/Documents/Bioinformatics/Week01/rst.txt", "w") do file
    write(file, join(string.(kmersDic[pattern] .- 1), " "))
end