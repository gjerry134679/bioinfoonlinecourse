function pattern2Num(dna::String)
    return pattern2Num.(collect(dna))
end

function pattern2Num(nt::Char)::Integer
    if nt == 'A'
        return 0
    elseif nt == 'C'
        return 1
    elseif nt == 'G'
        return 2
    else
        return 3
    end
end

function num2Pattern(id::Integer, k::Integer)::String
    rst = join(int2Nt.(dec2Qua(id)), "");
    rst = join([repeat('A', k - length(rst)), rst], "");
    return rst
end

#function dec2Qua(num::Integer, rst::Array{Integer, 1})::Array{Integer, 1}
    #println(num)
#    if num < 4
#        return Array{Integer, 1}([num])
#    else
#        return append!(dec2Qua(num ÷ 4, rst), num % 4)
#    end
#end

function dec2Qua(num::Integer)::Array{Integer, 1}
    #println(num)
    rst = Array{Integer, 1}()
    if num < 4
        return [num]
    else
        return append!(dec2Qua(num ÷ 4), num % 4)
    end
end


function int2Nt(i::Integer)::Char
    if i == 0
        return 'A'
    elseif i == 1
        return 'C'
    elseif i == 2
        return 'G'
    else
        return 'T'
    end
end

function qua2Dec(num)
    rst = 0;
    for i = 1:length(num)
        rst += num[i]*4^(length(num) - i)
    end
    return rst
end

function computingFreq(dna::String, k::Integer)::Array{Integer,1}
    feqArray = zeros(Integer, 4^k);
    for i = 1:(length(dna) - k + 1)
        feqArray[qua2Dec(pattern2Num(dna[i:i+k-1]))+1] += 1;
    end
    return feqArray
end

file = open("/home/chang/Documents/Bioinformatics/Week01/testSeq.txt", "r")
dna = readline(file);
k   = parse(Int, readline(file));
close(file);

rst = join(computingFreq(dna, k), " ");
open("/home/chang/Documents/Bioinformatics/Week01/rst.txt", "w") do file
    write(file, rst);
end
