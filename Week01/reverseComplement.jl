function comp(nt::Char)::Char
    if nt == 'A'
        return 'T'
    elseif nt == 'T'
        return 'A'
    elseif nt == 'C'
        return 'G'
    else
        return 'C'
    end
end

function reverseComSeq(dna::String)::String
    cdna = Array{Char}(undef, length(dna))
    return join(reverse(comp.(collect(dna))), "")
end

function kmers(dna::String, k::Integer)::Dict{String, Integer}
    dict = Dict{String, Integer}()
    for i = 1:(length(dna) -k+1)
        kmer = dna[i:(i+k-1)]
        if haskey(dict, kmer)
            dict[kmer] = dict[kmer] + 1
        else
            dict[kmer] = 1
        end
    end
    return dict
end

function getKmersByTimes(kmersDic::Dict{String, Integer}, times::Integer)::Array{String, 1}
    kmers  = Array{String}(undef, 0)
    for (key, value) in kmersDic
        if value == times
            push!(kmers, key)
        end
    end
    return kmers
end

function getMostFreqKmers(dna::String, k::Integer)::Array{String, 1}
    dnaKmers  = kmers(dna, k)
    return getKmersByTimes(dnaKmers, maximum(values(dnaKmers)))
end


file = open("/home/chang/Documents/Bioinformatics/Week01/testSeq.txt", "r")
dna  = read(file, String)
close(file)

open("/home/chang/Documents/Bioinformatics/Week01/rst.txt", "w") do file
    write(file, reverseComSeq(dna))
end

