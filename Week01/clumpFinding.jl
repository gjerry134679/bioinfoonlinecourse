function clumpFinding(dna::String, k::Integer, L::Integer, t::Integer)
    kmerDict = Dict{String, Integer}()
    rst = Array{String, 1}()
    subseq   = dna[1:L]
    frstKmer = subseq[1:k]

    for i = 1:(L-k+1)
        kmer = dna[i:i+k-1]
        if haskey(kmerDict, kmer)
            kmerDict[kmer] += 1
        else
            kmerDict[kmer] = 1
        end
    end
    
    for (key, value) = kmerDict
        if value == t
            push!(rst, key)
            #println(key)
        end
    end

    for i = 2:(length(dna)-L+1)
        kmerDict[frstKmer] -= 1
        subseq   = dna[i:i+L-1]
        frstKmer = subseq[1:k]
        lstKmer = subseq[L-k+1:L]

        if haskey(kmerDict, lstKmer)
            kmerDict[lstKmer] += 1
            if kmerDict[lstKmer] == t
                push!(rst, lstKmer)
                #println(lstKmer)
            end
        else 
            kmerDict[lstKmer] = 1
        end
    end

        return unique!(rst)
end

file = open("/home/chang/Documents/Bioinformatics/Week01/E_coli.txt", "r");
genome = read(file);
close(file);
genome = String(genome);

# (k, L, t) = parse.(Int, split(pars, " "))
(k, L, t) = (9, 500, 3);
@time rst = clumpFinding(genome, k, L, t)
println(sort(rst))

open("/home/chang/Documents/Bioinformatics/Week01/rst.txt", "w") do file
    write(file, join(clumpFinding(dna, k, L, t), " "))
end